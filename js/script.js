var $root = $('html, body');

$('a[href^="#"]').click(function () {
    $root.animate({
        scrollTop: $('[name="' + $.attr(this, 'href').substr(1) + '"]').offset().top
    }, 500);

    return false;
});

$('#toggleMe').css('top', $('.popular-posts').offset().top);
$('#toggleMe').click(function () {
    $('.popular-posts').slideToggle();
});

$('#up').click(function () {
    $root.animate({
        scrollTop: 0
    }, 500);
});

$(window).scroll(function () {
    if ($(this).scrollTop() > $(this).height()) {
        $('#up').show();
    }
    if ($(this).scrollTop() === 0) {
        $('#up').hide();
    }
});